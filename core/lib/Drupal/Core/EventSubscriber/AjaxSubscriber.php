<?php

namespace Drupal\Core\EventSubscriber;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * @file
 *
 * Definition of Drupal\Core\EventSubscriber\HtmlSubscriber;
 */

/**
 * Main subscriber for AJAX-type HTTP responses.
 */
class AjaxSubscriber implements EventSubscriberInterface {

  /**
   * Determines if we are dealing with an AJAX-style response.
   *
   * @param GetResponseEvent $event
   *   The Event to process.
   * @return boolean
   *   True if it is an event we should process as AJAX, False otherwise.
   */
  protected function isAjaxRequestEvent(GetResponseEvent $event) {
    return $event->getRequest()->isXmlHttpRequest();
  }

  protected function createAjaxResponse(GetResponseEvent $event) {
    $response = new Response();

    // Browsers do not allow JavaScript to read the contents of a user's local
    // files. To work around that, the jQuery Form plugin submits forms containing
    // a file input element to an IFRAME, instead of using XHR. Browsers do not
    // normally expect JSON strings as content within an IFRAME, so the response
    // must be customized accordingly.
    // @see http://malsup.com/jquery/form/#file-upload
    // @see Drupal.ajax.prototype.beforeSend()
    if ($event->getRequest()->request->get('ajax_iframe_upload', FALSE)) {
      $response->headers->set('Content-Type', 'text/html; charset=utf-8');
    }
    else {
      $response->headers->set('Content-Type', 'application/json; charset=utf-8');
    }

    return $response;
  }

  /**
   * Processes an AccessDenied exception into an HTTP 403 response.
   *
   * @param GetResponseEvent $event
   *   The Event to process.
   */
  public function onAccessDeniedException(GetResponseEvent $event) {
    if ($this->isAjaxRequestEvent($event) && $event->getException() instanceof AccessDeniedHttpException) {
      $response = $this->createAjaxResponse($event);
      $response->setStatusCode(403, 'Access Denied');
      $event->setResponse($response);
    }
  }

  /**
   * Processes a NotFound exception into an HTTP 404 response.
   *
   * @param GetResponseEvent $event
   *   The Event to process.
   */
  public function onNotFoundHttpException(GetResponseEvent $event) {
    if ($this->isAjaxRequestEvent($event) && $event->getException() instanceof NotFoundHttpException) {
      $response = $this->createAjaxResponse($event);
      $response->setStatusCode(404, 'Not Found');
      $event->setResponse($response);
    }
  }

  /**
   * Processes a MethodNotAllowed exception into an HTTP 405 response.
   *
   * @param GetResponseEvent $event
   *   The Event to process.
   */
  public function onMethodAllowedException(GetResponseEvent $event) {
    if ($this->isAjaxRequestEvent($event) && $event->getException() instanceof MethodNotAllowedException) {
      $response = $this->createAjaxResponse($event);
      $response->setStatusCode(405, 'Method Not Allowed');
      $event->setResponse($response);
    }
  }

  /**
   * Processes a successful controller into an HTTP 200 response.
   *
   * Some controllers may not return a response object but simply the body of
   * one.  The VIEW event is called in that case, to allow us to mutate that
   * body into a Response object.  In particular we assume that the return
   * from an JSON-type response is a JSON string, so just wrap it into a
   * Response object.
   *
   * @param GetResponseEvent $event
   *   The Event to process.
   */
  public function onView(GetResponseEvent $event) {
    if ($this->isAjaxRequestEvent($event)) {
      $page_callback_result = $event->getControllerResult();

      $response = $this->createAjaxResponse($event);
      $response->setContent(ajax_render($page_callback_result));

      $event->setResponse($response);
    }
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::EXCEPTION][] = array('onNotFoundHttpException', 1);
    $events[KernelEvents::EXCEPTION][] = array('onAccessDeniedException', 1);
    $events[KernelEvents::EXCEPTION][] = array('onMethodAllowedException', 1);

    $events[KernelEvents::VIEW][] = array('onView', 1);

    return $events;
  }
}
